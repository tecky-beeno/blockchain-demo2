import { ChainNode } from '../src/node'

let node1 = new ChainNode('node1')
node1.startServer(9001)

let node2 = new ChainNode('node2')
node2.startServer(9002)

let node3 = new ChainNode('node3')
node3.startServer(9003)

node1.addPeer({ name: 'node2', urls: ['http://localhost:9002'] })

node2.addPeer({ name: 'node1', urls: ['http://localhost:9001'] })
node2.addPeer({ name: 'node3', urls: ['http://localhost:9003'] })

node3.addPeer({ name: 'node2', urls: ['http://localhost:9002'] })

let nodes = [node1, node2, node3]
nodes.forEach(node => {
  node.syncWithPeer().then(() => {
    node.startMine()
  })
})
