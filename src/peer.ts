import fetch from 'node-fetch'
import { hashBlock } from './hash'
import { ChainNode } from './node'
import { Block, Inv } from './types'

const TIMEOUT = 1000 * 10

export type Peer = {
  name: string
  urls: string[]
}
export class PeerBook {
  constructor(private node: ChainNode) {}

  peers = new Map<string, Peer>()

  addPeer(peer: Peer) {
    this.peers.set(peer.name, peer)
  }

  getInv(height: number, cb: (inv: Inv) => void) {
    let ps: Promise<any>[] = []
    this.peers.forEach(peer => {
      peer.urls.forEach(url => {
        ps.push(
          new Promise<void>((resolve, reject) => {
            let timer = setTimeout(() => {
              this.node.log('Timeout when getInv from peer:', {
                name: peer.name,
                url,
              })
              resolve()
            }, TIMEOUT)
            fetch(url + '/inv?height=' + height)
              .then(res => res.json())
              .then(json => {
                if (json.error) {
                  throw new Error(json.error)
                }
                if (json.inv) {
                  return json.inv as Inv
                }
                throw new Error('Invalid response')
              })
              .then(inv => {
                this.node.log('Got Inv from peer:', {
                  name: peer.name,
                  url,
                  inv,
                })
                cb(inv)
              })
              .catch(error => {
                this.node.log('Failed to getInv from peer:', {
                  name: peer.name,
                  url,
                  error,
                })
              })
              .finally(() => {
                clearTimeout(timer)
                resolve()
              })
          }),
        )
      })
    })
    if (ps.length === 0) {
      throw new Error('No peers available')
    }
    return Promise.all(ps)
  }

  getBlockByHeight(height: number, cb: (block: Block) => void) {
    let ps: Promise<any>[] = []
    this.peers.forEach(peer => {
      peer.urls.forEach(url => {
        ps.push(
          new Promise<void>((resolve, reject) => {
            setTimeout(resolve, TIMEOUT)
            fetch(url + '/block?height=' + height)
              .then(res => res.json())
              .then(json => {
                if (json.error) {
                  throw new Error(json.error)
                }
                if (json.block) {
                  return json.block as Block
                }
                throw new Error('Invalid response')
              })
              .then(block => {
                if (block.height !== height) {
                  throw new Error('Invalid block height')
                }
                cb(block)
              })
              .catch(error => {
                this.node.log('Failed to get block from peer:', {
                  name: peer.name,
                  url,
                  error,
                  height,
                })
              })
              .finally(() => resolve())
          }),
        )
      })
    })
    if (ps.length === 0) {
      throw new Error('No peers available')
    }
    return Promise.race(ps)
  }

  getBlockByHash(hash: string, cb: (block: Block) => void) {
    let ps: Promise<any>[] = []
    this.peers.forEach(peer => {
      peer.urls.forEach(url => {
        ps.push(
          new Promise<void>((resolve, reject) => {
            setTimeout(resolve, TIMEOUT)
            fetch(url + '/block?hash=' + hash)
              .then(res => res.json())
              .then(json => {
                if (json.error) {
                  throw new Error(json.error)
                }
                if (json.block) {
                  return json.block as Block
                }
                throw new Error('Invalid response')
              })
              .then(block => {
                if (hashBlock(block) !== hash) {
                  this.node.log('Invalid block hash:', { hash, block })
                  throw new Error('Invalid block hash')
                }
                cb(block)
              })
              .catch(error => {
                if (String(error).includes('Block not found')) {
                  // the peer delete the block after jump between forks
                  return
                }
                this.node.log('Failed to get block from peer:', {
                  name: peer.name,
                  url,
                  error,
                  hash,
                })
              })
              .finally(() => resolve())
          }),
        )
      })
    })
    if (ps.length === 0) {
      throw new Error('No peers available')
    }
    return Promise.race(ps)
  }

  broadcastBlock(block: Block) {
    let body = JSON.stringify(block)
    this.peers.forEach(peer =>
      peer.urls.forEach(url => {
        fetch(url + '/block', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body,
        })
      }),
    )
  }
}
