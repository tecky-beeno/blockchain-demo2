import { DB } from './db'
import path from 'path'
import { Block, Inv } from './types'
import { hashBlock, ZERO_HASH } from './hash'
import { Server } from './server'
import { makeLog } from './log'
import { Peer, PeerBook } from './peer'

const MINE_INTERVAL = 500

export class ChainNode {
  private db = new DB(path.join('data', this.key + '.db'), this)
  private nonce = 0
  private mineTimer: any
  private server?: Server
  public readonly log = makeLog(this.key)
  private peerBook = new PeerBook(this)

  constructor(public key: string) {
    this.initDB()
  }

  private initDB() {
    this.clearInvalidBlock()
    if (this.db.getPrevBlock()) {
      return
    }
    let genesisBlock: Block = {
      height: 1,
      prev_block_hash: ZERO_HASH,
      txn_list: [],
      nonce: 1,
      miner: 'genesis',
      reward: 0,
      timestamp: new Date('2022-02-08T09:00:21.601Z').getTime(),
    }
    hashBlock(genesisBlock)
    this.db.appendBlock(genesisBlock)
  }

  private clearInvalidBlock() {
    this.db.clearInvalidBlock()
  }

  startServer(port: number) {
    if (this.server) {
      return
    }
    this.server = new Server(this)
    this.server.start(port)
  }

  stopServer() {
    this.server?.stop()
    delete this.server
  }

  async syncWithPeer() {
    this.initDB()
    let lastBlock = this.db.getPrevBlock()
    this.log('syncWithPeer, last block:', lastBlock)
    let blockBuffer: Record<string, 'downloading' | Block> = {}
    const waitForBlockDownloads = () =>
      new Promise<void>((resolve, reject) => {
        const checkProgress = () => {
          if (
            Object.values(blockBuffer).some(block => block === 'downloading')
          ) {
            // still downloading
            return
          }
          resolve()
        }
        const downloadBlock = (hash: string) => {
          if (hash in blockBuffer || hash === ZERO_HASH) {
            checkProgress()
            return
          }
          // this.log('downloading block:', { hash })
          blockBuffer[hash] = 'downloading'
          this.peerBook
            .getBlockByHash(hash, block => {
              blockBuffer[hash] = block
              if (this.db.getBlockByHash(block.prev_block_hash)) {
                return
              }
              downloadBlock(block.prev_block_hash)
            })
            .then(() => {
              if (blockBuffer[hash] === 'downloading') {
                delete blockBuffer[hash]
              }
            })
            .finally(() => checkProgress())
        }
        this.peerBook
          .getInv(lastBlock.height, inv => {
            if (inv.last.height <= lastBlock.height) return
            downloadBlock(inv.last.hash)
          })
          .then(() => checkProgress())
      })

    await waitForBlockDownloads()
    this.log('sync many blocks from peer?', Object.keys(blockBuffer).length)

    // build up a tree data structure to help lookup the length behind each block
    type BlockNode = {
      block?: Block
      // hash -> tree node
      tails: Record<string, BlockNode>
    }
    // hash -> tree node
    let nodes: Record<string, BlockNode> = {}
    function getNode(hash: string): BlockNode {
      if (hash in nodes) {
        return nodes[hash]
      }
      return (nodes[hash] = { tails: {} })
    }
    Object.entries(blockBuffer).forEach(([hash, block]) => {
      if (block === 'downloading') {
        delete blockBuffer[hash]
        return
      }
      let node = getNode(hash)
      node.block = block
      let parentNode = getNode(block.prev_block_hash)
      parentNode.tails[hash] = node
    })
    this.log('built block tree:', Object.keys(nodes).length)

    const countSelfAndTails = (hash: string, node: BlockNode): number => {
      let count = 1
      Object.entries(node.tails).forEach(([tailHash, tailNode]) => {
        let tailCount = countSelfAndTails(tailHash, tailNode)
        count = Math.max(count, 1 + tailCount)
      })
      return count
    }

    type FinalBlockNode = {
      block: Block
      hash: string
      height: number
      tail: FinalBlockNode | null
    }
    // hash -> tree node
    let finalNodes: Record<string, FinalBlockNode> = {}
    const buildFinalNode = (
      hash: string,
      node: BlockNode,
    ): FinalBlockNode | null => {
      if (hash in finalNodes) return finalNodes[hash]
      let block = node.block
      if (!block) return null
      let longestTail: {
        node: BlockNode
        hash: string
        length: number
      } | null = null
      Object.entries(node.tails).forEach(([tailHash, tailNode]) => {
        let count = countSelfAndTails(tailHash, tailNode)
        if (!longestTail || count >= longestTail.length) {
          longestTail = {
            node: tailNode,
            hash: tailHash,
            length: count,
          }
        }
      })
      let finalNode: FinalBlockNode = {
        block,
        hash,
        height: block.height,
        tail: null,
      }
      // this.log('longestTail:', { height: block.height, longestTail })
      if (longestTail) {
        let { hash, node } = longestTail as any
        finalNode.tail = buildFinalNode(hash, node)
      }
      return (finalNodes[hash] = finalNode)
    }
    let minHeight = lastBlock.height + 1
    let minNode: FinalBlockNode | null = null
    Object.entries(nodes).forEach(([hash, node]) => {
      let finalNode = buildFinalNode(hash, node)
      let height = node.block?.height
      if (height && height <= minHeight) {
        minHeight = height
        minNode = finalNode
      }
    })

    this.log('finalNodes:', Object.keys(finalNodes).length)

    this.log('minHeight:', minHeight)
    this.log('minNode:', minNode)

    let lastHeight = lastBlock.height
    let currentNode: FinalBlockNode | null = minNode
    while (currentNode) {
      let finalNode = currentNode as FinalBlockNode
      let block = finalNode.block
      while (lastHeight >= block.height) {
        this.log('revokeBlockByHeight:', lastHeight)
        this.db.revokeBlockByHeight(lastHeight)
        lastHeight--
      }
      this.log('import block:', block)
      let result = this.receiveBlock(block)
      if (result !== 'accept') {
        this.log('failed to import block:', result)
        throw new Error('Failed in syncWithPeer')
      }
      lastHeight = block.height
      currentNode = finalNode.tail
    }

    this.log('finish sync')
  }

  isMining() {
    return !!this.mineTimer
  }

  startMine() {
    this.mineTimer = setTimeout(() => this.mine())
  }

  stopMine() {
    clearTimeout(this.mineTimer)
  }

  mine() {
    let prev_block = this.db.getPrevBlock()
    let prev_block_hash = prev_block.hash!
    let height = prev_block.height + 1
    this.nonce++
    let block: Block = {
      height,
      prev_block_hash,
      txn_list: [],
      nonce: this.nonce,
      miner: this.key,
      reward: this.getBlockReward(height),
      timestamp: Date.now(),
    }
    let max_hash = this.getDifficulty()
    let block_hash = hashBlock(block)
    // this.log('mine:', {
    //   height: block.height,
    //   nonce: block.nonce,
    // })
    if (block_hash < max_hash) {
      this.receiveBlock(block)
    }
    setTimeout(() => this.mine(), MINE_INTERVAL)
  }

  getDifficulty() {
    return '0fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff'
  }

  getBlockReward(height: number) {
    return 1
  }

  verifyBlock(
    block: Block,
  ):
    | { accept: true; hash: string }
    | { accept: false; reason: 'future' | 'invalid' | 'old' | 'fork' } {
    if (block.timestamp > Date.now()) {
      return { accept: false, reason: 'invalid' }
    }
    if (block.reward !== this.getBlockReward(block.height)) {
      return { accept: false, reason: 'invalid' }
    }
    let prev_block = this.db.getPrevBlock()
    if (block.height > prev_block.height + 1) {
      return { accept: false, reason: 'future' }
    }
    if (block.height !== prev_block.height + 1) {
      return { accept: false, reason: 'old' }
    }
    if (prev_block.hash !== block.prev_block_hash) {
      return { accept: false, reason: 'fork' }
    }
    let block_hash = hashBlock(block)
    let max_hash = this.getDifficulty()
    if (block_hash < max_hash) {
      return { accept: true, hash: block_hash }
    }
    return { accept: false, reason: 'invalid' }
  }

  receiveBlock(block: Block) {
    // this.log('receive block:', block)
    let result = this.verifyBlock(block)
    if (!result.accept) {
      return result.reason
    }
    let block_hash = result.hash
    this.log('append block:', { block_hash, block })
    block.hash = block_hash
    this.db.appendBlock(block)
    this.peerBook.broadcastBlock(block)
    return 'accept' as const
  }

  getInv(height: number): Inv {
    let lastBlock = this.db.getPrevBlock()
    let block = this.db.getBlockByHeight(height)
    return {
      last: { height: lastBlock.height, hash: lastBlock.hash! },
      block: block ? { height: block.height, hash: block.hash! } : null,
    }
  }

  getBlockByHeight(height: number) {
    return this.db.getBlockByHeight(height)
  }

  getBlockByHash(hash: string) {
    return this.db.getBlockByHash(hash)
  }

  addPeer(peer: Peer) {
    this.peerBook.addPeer(peer)
  }
}
