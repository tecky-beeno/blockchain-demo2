import express from 'express'
import { ChainNode } from './node'
import http from 'http'
import { print } from 'listening-on'
import { ZERO_HASH } from './hash'
import { Block } from './types'

export class Server {
  private app = express()
  private httpServer?: http.Server

  constructor(private node: ChainNode) {
    this.app.use(express.json())

    this.app.post('/block', async (req, res) => {
      res.end()
      try {
        let result = node.receiveBlock(req.body)
        if (result === 'future') {
          this.node.log('received future blocks, try to catch up:', req.body)
          if (node.isMining()) {
            node.stopMine()
            await node.syncWithPeer()
            node.startMine()
          } else {
            await node.syncWithPeer()
          }
        }
      } catch (error) {}
    })

    this.app.get('/inv', (req, res) => {
      let height = +req.query.height!
      if (!height) {
        res.status(400).json({ error: 'Invalid height in request query' })
        return
      }
      try {
        let inv = this.node.getInv(height)
        res.json({ inv })
      } catch (error) {
        res.status(500).json({ error: String(error) })
      }
    })

    this.app.get('/block', (req, res) => {
      let height = +req.query.height!
      let hash = req.query.hash

      try {
        let block: Block | null
        if (hash) {
          if (typeof hash !== 'string' || hash.length !== ZERO_HASH.length) {
            res.status(400).json({
              error: 'Invalid hash in request query',
              detail: 'expect a sha256 hash in hex format',
            })
            return
          }
          block = node.getBlockByHash(hash)
        } else if (height) {
          block = node.getBlockByHeight(height)
        } else {
          res
            .status(400)
            .json({ error: 'Missing height or hash in request query' })
          return
        }
        if (!block) {
          res.status(404).json({ error: 'Block not found' })
        } else {
          res.json({ block })
        }
      } catch (error) {
        res.status(500).json({ error: String(error) })
      }
    })
  }

  start(port: number) {
    if (this.httpServer) {
      return
    }
    this.httpServer = this.app.listen(port, () => {
      let log = console.log
      console.log = this.node.log
      print(port)
      console.log = log
    })
  }

  stop() {
    this.httpServer?.close()
    delete this.httpServer
  }
}
