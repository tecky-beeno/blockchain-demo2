export type Block = {
  height: number
  prev_block_hash: string
  txn_list: Txn[]
  nonce: number
  miner: string
  reward: number
  timestamp: number
  hash?: string
}

export type Txn = TransferTxn

export type TransferTxn = {
  type: 'transfer'
  from: string
  to: string
  amount: number
}

export type Inv = {
  last: { height: number; hash: string }
  block: null | { height: number; hash: string }
}
