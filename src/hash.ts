import crypto from 'crypto'
import { Block } from './types'

let HASH_SIZE = 256
let BYTE = 8

export let ZERO_HASH = Buffer.alloc(HASH_SIZE / BYTE)
  .fill(0)
  .toString('hex')

function hashObject(object: object): string {
  let hash = crypto.createHash('sha256')
  hash.write(JSON.stringify(object))
  return hash.digest().toString('hex')
}

export function hashBlock(block: Block): string {
  delete block.hash
  return (block.hash = hashObject(block))
}
