import { newDB } from 'better-sqlite3-schema'
import { ChainNode } from './node'
import { Block } from './types'

export class DB {
  private db = newDB({
    path: this.path,
    migrate: {
      table: 'migration',
      migrationsPath: './migrations',
    },
  })
  private select_block_by_height = this.db.prepare(
    /* sql */ `select * from block where height = ? limit 1`,
  )
  private select_block_by_hash = this.db.prepare(
    /* sql */ `select * from block where hash = ? limit 1`,
  )
  private select_last_block = this.db.prepare(
    /* sql */ `select * from block order by height desc limit 1`,
  )
  private delete_block_by_height = this.db.prepare(
    /* sql */ `delete from block where height = ?`,
  )

  /* for clearInvalidBlock */
  private select_missing_blocks = this.db.prepare(/* sql */ `
    select height
    from block
    where height <> 1
      and prev_block_hash not in (
      select hash from block
    )`)

  constructor(public readonly path: string, private node: ChainNode) {}

  clearInvalidBlock() {
    for (;;) {
      let rows = this.select_missing_blocks.all()
      if (rows.length === 0) return
      for (let row of rows) {
        this.node.log('clear invalid block:', row)
        this.revokeBlockByHeight(row.height)
      }
    }
  }

  getBlockByHeight(height: number): Block | null {
    return this.decodeBlock(this.select_block_by_height.get(height))
  }

  getBlockByHash(hash: string): Block | null {
    return this.decodeBlock(this.select_block_by_hash.get(hash))
  }

  appendBlock(block: Block) {
    if (!block.hash) {
      throw new Error('missing hash in block')
    }
    this.db.insert('block', {
      ...block,
      txn_list: JSON.stringify(block.txn_list),
    })
  }

  revokeBlockByHeight(height: number) {
    this.delete_block_by_height.run(height)
  }

  getPrevBlock(): Block {
    return this.decodeBlock(this.select_last_block.get())!
  }

  private decodeBlock(block: Block | null): Block | null {
    if (!block) {
      return null
    }
    block.txn_list = JSON.parse(block.txn_list as any)
    return block
  }
}
