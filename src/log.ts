import debug from 'debug'

export function makeLog(name: string) {
  let log = debug(name)
  log.enabled = true
  return log
}

export type Log = ReturnType<typeof makeLog>
