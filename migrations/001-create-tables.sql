-- Up
create table if not exists block (
	height integer primary key
, prev_block_hash text
, hash text
, txn_list text
, nonce integer
, miner text
, reward integer
, timestamp integer
);

-- Down
drop table if exists block;